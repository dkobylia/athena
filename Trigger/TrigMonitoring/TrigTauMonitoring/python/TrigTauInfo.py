# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

# Usage:
# from TrigTauMonitoring.TrigTauInfo import TrigTauInfo

import ROOT
TrigTauInfo = ROOT.TrigTauInfo