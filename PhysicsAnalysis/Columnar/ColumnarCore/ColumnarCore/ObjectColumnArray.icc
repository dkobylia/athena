/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#include <ColumnarCore/ColumnarTool.h>
#include <ColumnarCore/ColumnAccessorDataArray.h>
#include <ColumnarCore/ObjectId.h>
#include <ColumnarCore/ObjectRange.h>
#include <ColumnarCore/ContainerId.h>
#include <cassert>

namespace columnar
{
  template<ContainerId CI>
  class AccessorTemplate<CI,ObjectColumn,ColumnAccessMode::input,ColumnarModeArray> final
  {
    /// Common Public Members
    /// =====================
  public:

    static_assert (ContainerIdTraits<CI>::isDefined, "ContainerId not defined, include the appropriate header");

    using CM = ColumnarModeArray;

    AccessorTemplate (ColumnarTool<CM>& columnBase,
                      const std::string& name)
      : m_columnBase (&columnBase),
        m_name (name)
    {
      columnBase.setContainerName (CI, name);
      if constexpr (ContainerIdTraits<CI>::isMutable)
        columnBase.setContainerName (ContainerIdTraits<CI>::constId, name);
      m_offsetsData = std::make_unique<ColumnAccessorDataArray> (&m_offsetsIndex, &m_offsetsData, &typeid (ColumnarOffsetType), ColumnAccessMode::input);
      columnBase.addColumn (name, m_offsetsData.get(), {.offsetName = (CI!=ContainerId::eventInfo ? numberOfEventsName : ""), .isOffset = true});
    }

    AccessorTemplate (AccessorTemplate&& that)
    {
      moveAccessor (m_offsetsIndex, m_offsetsData, that.m_offsetsIndex, that.m_offsetsData);
    }

    AccessorTemplate& operator = (AccessorTemplate&& that)
    {
      if (this != &that)
        moveAccessor (m_offsetsIndex, m_offsetsData, that.m_offsetsIndex, that.m_offsetsData);
      return *this;
    }

    AccessorTemplate (const AccessorTemplate&) = delete;
    AccessorTemplate& operator = (const AccessorTemplate&) = delete;

    ObjectRange<CI,CM> operator() (ObjectId<ContainerId::eventContext,CM> eventId) const
      requires (ContainerIdTraits<CI>::perEventRange)
    {
      auto *offsets = static_cast<const ColumnarOffsetType*>(eventId.getData()[m_offsetsIndex]);
      return ObjectRange<CI,CM> (eventId.getData(), offsets[eventId.getIndex()], offsets[eventId.getIndex() + 1]);
    }

    ObjectRange<CI,CM> operator() (ObjectRange<ContainerId::eventContext,CM> eventRange) const
      requires (ContainerIdTraits<CI>::perEventRange)
    {
      auto *offsets = static_cast<const ColumnarOffsetType*>(eventRange.getData()[m_offsetsIndex]);
      return ObjectRange<CI,CM> (eventRange.getData(), offsets[eventRange.beginIndex()], offsets[eventRange.endIndex()]);
    }

    ObjectId<CI,CM> operator() (ObjectId<ContainerId::eventContext,CM> eventId) const
      requires (ContainerIdTraits<CI>::perEventId)
    {
      return ObjectId<CI,CM> (eventId.getData(), eventId.getIndex());
    }

    ObjectRange<CI,CM> operator() (ObjectRange<ContainerId::eventContext,CM> eventRange) const
      requires (ContainerIdTraits<CI>::perEventId)
    {
      return ObjectRange<CI,CM> (eventRange.getData(), eventRange.beginIndex(), eventRange.endIndex());
    }



    /// Private Members
    /// ===============
  private:

    ColumnarTool<CM> *m_columnBase = nullptr;

    std::string m_name;
    unsigned m_offsetsIndex = 0;
    std::unique_ptr<ColumnAccessorDataArray> m_offsetsData;
  };
}
