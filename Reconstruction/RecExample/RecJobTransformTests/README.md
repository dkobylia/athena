RecJobTransformTests
========================

Various tests of the full reconstruction transform Reco_tf.py for various data years, MC campaigns, and special detector configurations.
Learn more at https://twiki.cern.ch/twiki/bin/view/AtlasComputing/ReconstructionDocumentation
For any issues, file a ticket at https://its.cern.ch/jira/projects/ATLASRECTS/